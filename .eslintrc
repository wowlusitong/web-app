{
  "parser": "babel-eslint",
  "extends": "airbnb",
  "rules": {
    "array-bracket-spacing": [2, "never"],
    "arrow-parens": 0,
    "block-spacing": [2, "never"],
    "class-methods-use-this": 0,
    "comma-dangle": [2, "never"],
    "computed-property-spacing": [2, "never"],
    "constructor-super": 2,
    "func-names": 0,
    "id-length": 0,
    "indent": [2, 2, SwitchCase: 1],
    "jsx-quotes": [2, "prefer-double"],
    "linebreak-style": [2, "unix"],
    "max-len": [1, 100, {ignoreTrailingComments: true, ignoreTemplateLiterals: true}],
    "new-cap": [2, {"newIsCap": true, "capIsNew": false}],
    "newline-per-chained-call": [1, {"ignoreChainWithDepth": 2}],
    "no-bitwise": ["error", {"allow": ["~"]}],
    "no-case-declarations": 2,
    "no-class-assign": 2,
    "no-confusing-arrow": ["error", {"allowParens": true}],
    "no-const-assign": 2,
    "no-console": 0,
    "no-empty-pattern": 2,
    "no-dupe-class-members": 2,
    "no-duplicate-imports": 0,
    "no-nested-ternary": 0,
    "no-param-reassign": 0,
    "no-plusplus": ["error", {"allowForLoopAfterthoughts": true}],
    "no-restricted-globals": ["error", "event", "Map", "Set"],
    "no-sequences": 0,
    "no-this-before-super": 2,
    "no-undefined": 2,
    "no-unused-expressions": 0,
    "no-unused-vars": ["error", { "varsIgnorePattern": "^_" }],
    "no-useless-concat": 2,
    "no-var": 2,
    "object-curly-spacing": [2, "never"],
    "prefer-arrow-callback": 2,
    "prefer-reflect": 1,
    "prefer-spread": 2,
    "quote-props": [2, "as-needed", {"keywords": false, "unnecessary": true, "numbers": false}],
    "require-yield": 2,
    "space-before-function-paren": [2, "never"],
    "space-in-parens": [2, "never"],
    "vars-on-top": 0,
    "arrow-body-style": 1,

    "import/prefer-default-export": 0,
    "import/no-duplicates": 2,
    "import/newline-after-import": 2,

    "jsx-a11y/href-no-hash": 0,
    "jsx-a11y/no-static-element-interactions": 1,
    "jsx-a11y/label-has-for": 1,

    "react/forbid-prop-types": 0,
    "react/no-find-dom-node": 1,
    "react/no-unused-prop-types": 1,
    "react/jsx-closing-bracket-location": [1, 'props-aligned'],
    "react/jsx-curly-spacing": [2, "never"],
    "react/jsx-filename-extension": ["error", {"extensions": [".js"]}],
    "react/jsx-first-prop-new-line": 0,
    "react/jsx-handler-names": [1, {
      "eventHandlerPrefix": 'handle|toggle'
    }],
    "react/jsx-indent-props": [2, 2],
    "react/jsx-no-bind": 0,
    "react/jsx-no-duplicate-props": 2,
    "react/jsx-no-target-blank": 1,
    "react/no-direct-mutation-state": 2,
    "react/no-multi-comp": [2, {"ignoreStateless": true}],
    "react/no-string-refs": 1,
    "react/prefer-es6-class": 1,
    "react/prefer-stateless-function": 1,
    "react/prop-types": 0,
    "react/sort-comp": 0
  },

  "env": {
    "node": true,
    "browser": true
  },

  "globals": {
    "ReactElement": false,
    "ReactClass": false
  },

  "settings": {
    "import/resolver": {
      "babel-module": {}
    }
  }
}
