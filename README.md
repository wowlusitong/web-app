# 使用方法
- 安装依赖 
```sh
yarn install 或者 npm install
```
- 启动项目
```sh
# 比如 API_SERVER_URL=http://localhost:8000 npm start
API_SERVER_URL=接口地址 npm start
```
