// Module declared on line 38 in .d.ts file
declare module "immutable/contrib/cursor" {
  declare var Immutable: $Exports<'immutable'>;
  declare interface Cursor extends Immutable.Seq<any,any> {
    cursor(subKeyPath: Array<any>): Cursor;
    cursor(subKey: any): Cursor;
    deref(notSetValue?: any): any;
    get(key: any, notSetValue?: any): any;
    getIn(keyPath: Array<any>, notSetValue?: any): any;
    getIn(keyPath: Immutable.Iterable<any,any>, notSetValue?: any): any;
    set(key: any, value: any): Cursor;
    set(value: any): Cursor;
    delete(key: any): Cursor;
    remove(key: any): Cursor;
    clear(): Cursor;
    update(updater: (value: any) => any): Cursor;
    update(key: any, updater: (value: any) => any): Cursor;
    update(key: any, notSetValue: any, updater: (value: any) => any): Cursor;
    merge(...iterables: Array<Immutable.Iterable<any,any>>): Cursor;
    merge(...iterables: Array<{
                               [key: string]: any;}>): Cursor;
    mergeWith(
      merger: (
        previous?: any, next?: any
      ) => any, ...iterables: Array<Immutable.Iterable<any,any>>
    ): Cursor;
    mergeWith(
      merger: (
        previous?: any, next?: any
      ) => any, ...iterables: Array<{

        [key: string]: any;
      }>
    ): Cursor;
    mergeDeep(...iterables: Array<Immutable.Iterable<any,any>>): Cursor;
    mergeDeep(...iterables: Array<{
                                   [key: string]: any;}>): Cursor;
    mergeDeepWith(
      merger: (
        previous?: any, next?: any
      ) => any, ...iterables: Array<Immutable.Iterable<any,any>>
    ): Cursor;
    mergeDeepWith(
      merger: (
        previous?: any, next?: any
      ) => any, ...iterables: Array<{

        [key: string]: any;
      }>
    ): Cursor;
    setIn(keyPath: Array<any>, value: any): Cursor;
    setIn(keyPath: Immutable.Iterable<any,any>, value: any): Cursor;
    push(...values: Array<any>): Cursor;
    pop(): Cursor;
    unshift(...values: Array<any>): Cursor;
    shift(): Cursor;
    deleteIn(keyPath: Array<any>): Cursor;
    deleteIn(keyPath: Immutable.Iterable<any,any>): Cursor;
    removeIn(keyPath: Array<any>): Cursor;
    removeIn(keyPath: Immutable.Iterable<any,any>): Cursor;
    updateIn(keyPath: Array<any>, updater: (value: any) => any): Cursor;
    updateIn(
      keyPath: Array<any>, notSetValue: any, updater: (value: any) => any
    ): Cursor;
    updateIn(
      keyPath: Immutable.Iterable<any,any>, updater: (value: any) => any
    ): Cursor;
    updateIn(
      keyPath: Immutable.Iterable<any,any>,
      notSetValue: any,
      updater: (
        value: any
      ) => any
    ): Cursor;
    mergeIn(
      keyPath: Immutable.Iterable<any,any>, ...iterables: Array<Immutable.Iterable<any,
                                                                any>>
    ): Cursor;
    mergeIn(
      keyPath: Array<any>, ...iterables: Array<Immutable.Iterable<any,any>>
    ): Cursor;
    mergeIn(
      keyPath: Array<any>, ...iterables: Array<{
                                                [key: string]: any;}>
    ): Cursor;
    mergeDeepIn(
      keyPath: Immutable.Iterable<any,any>, ...iterables: Array<Immutable.Iterable<any,
                                                                any>>
    ): Cursor;
    mergeDeepIn(
      keyPath: Array<any>, ...iterables: Array<Immutable.Iterable<any,any>>
    ): Cursor;
    mergeDeepIn(
      keyPath: Array<any>, ...iterables: Array<{
                                                [key: string]: any;}>
    ): Cursor;
    withMutations(mutator: (mutable: any) => any): Cursor;
  }
  declare function from(collection: Immutable.Collection<any,any>,
  onChange: (
    newValue: any, oldValue?: any, keyPath?: Array<any>
  ) => any): Cursor;
  declare function from(collection: Immutable.Collection<any,any>,
  keyPath: Array<any>,
  onChange: (
    newValue: any, oldValue?: any, keyPath?: Array<any>
  ) => any): Cursor;
}

