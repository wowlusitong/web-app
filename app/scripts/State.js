/* @flow */

import {Map} from 'immutable';
import {createStore} from 'redux';

import {makeRootReducer} from '~/utils/reduxCursor';
import type {KeyPath} from '~/utils/reduxCursor';

const {rootReducer, addCursorHandler} = makeRootReducer();
const {getState, subscribe, dispatch} = createStore(rootReducer, new Map());

export function observe(paths: Array<KeyPath>, listener: Function): Function {
  let state = getState();
  return subscribe(() => {
    const newState = getState();
    if (paths.some(path => state.getIn(path) !== newState.getIn(path))) {
      listener();
    }
    state = newState;
  });
}

export {dispatch, getState, addCursorHandler};
