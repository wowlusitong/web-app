/* @flow */

import {dispatch} from '~/State';
import {ActionTypes} from '~/Constants';
import Blog from '~/services/Blog';

export async function loadBlogs() {
  const blogs = await new Blog().index();
  dispatch({
    type: ActionTypes.LOAD_BLOG_SUCCESS,
    blogs
  });
}

export async function increase(payload) {
  await new Blog().update(payload);
}
