/* @flow */

import * as React from 'react';

class App extends React.Component<{
  children: React.Element<any>;
  location: Object;
}> {
  getRouteName(): string {
    const {location: {pathname}} = this.props;
    const name = pathname.split('/')[1];
    return name || 'dashboard';
  }

  getFullRouteName(): string {
    const {location: {pathname}} = this.props;
    const names = pathname.split('/');
    return `page-${names[1]}-${names[names.length - 1]}`;
  }

  render(): React.Node {
    return (
      <div className={`page page-${this.getRouteName()} ${this.getFullRouteName()}`}>
        {this.props.children}
      </div>
    );
  }
}

export default App;
