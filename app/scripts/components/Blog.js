/* @flow */

import * as React from 'react';
import {compose} from 'recompose';
import {List} from 'immutable';

import {watchStores, beforeMount, renderLoading} from '~/components/utils/wrappers';
import BlogStore from '~/stores/Blog';
import {loadBlogs} from '~/actions/Blog';
import {Blog as BlogRecord} from '~/models/Blog';
import BlogItem from '~/components/blog/Item';

type Props = {
  blogs: List<BlogRecord>;
}

class Blog extends React.PureComponent<Props> {
  render() {
    const {blogs} = this.props;
    return (
      <div className="blogs-wrap">
        {blogs.map(blog =>
          <BlogItem key={blog.id} blog={blog} />
        )}
      </div>
    );
  }
}

export default compose(
  beforeMount(() => {
    loadBlogs();
  }),
  watchStores([BlogStore], () => ({
    blogs: BlogStore.getBlogs()
  })),
  renderLoading()
)(Blog);
