/* @flow */

import * as React from 'react';

import {Item, Header, Title, Button} from '~/components/blog/elements';
import {Blog} from '~/models/Blog';
import {increase} from '~/actions/Blog';

type Props = {
  blog: Blog
}
type State = {
  isShow: boolean;
}

export default class BlogItem extends React.Component<Props, State> {
  constructor(props: Props) {
    super(props);
    this.state = {
      isShow: false
    };
  }

  toggleContent(event) {
    event.preventDefault();

    this.setState({
      isShow: !this.state.isShow
    });
  }

  handleClick(event) {
    const {title, id: blog_id} = this.props.blog;
    if (event.target.getAttribute('target') === '_blank') {
      increase({title, blog_id});
    }
  }

  render() {
    const {blog: {link, title, content}} = this.props;
    const {isShow} = this.state;

    return (
      <Item>
        <Header onClick={this.handleClick.bind(this)}>
          <Title>
            <a href={link} target="_blank">{title}</a>
          </Title>
          <Button href={link} target="_blank">新窗口打开</Button>
          <Button href="#" onClick={this.toggleContent.bind(this)}>
            {isShow ? '隐藏文章' : '显示文章'}
          </Button>
        </Header>
        {isShow &&
          <div>
            <hr />
            <div dangerouslySetInnerHTML={{__html: content}} />
          </div>
        }
      </Item>
    );
  }
}
