import styled from 'styled-components';

export const Item = styled.div`
  border-bottom: 1px #E8E8E8 solid;
  padding: 15px;
  display: flex;
  flex-flow: column nowrap;

  &:last-child {
    border-bottom: none;
  }
`;

export const Header = styled.div`
  display: flex;
`;
export const Title = styled.div`
  flex: 8;
  a {
    color: #000;
  }
`;
export const Button = styled.a`
  flex: 1;
  color: #2592FC;
  font-size: 14px;
`;
