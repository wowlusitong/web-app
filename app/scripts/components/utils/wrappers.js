/* @flow */

/* eslint-disable react/no-multi-comp */

import * as React from 'react';
import {compose, lifecycle, branch, doOnReceiveProps} from 'recompose';
import createElement from 'recompose/createElement';

import Store from '~/stores/Store';
import Loading from '~/services/Loading';


export type Wrapper = (Component: React.ComponentType<any>) => React.ComponentType<any>;


export function watchStores(stores: Array<Store>, callback: ((props: Object) => Object)): Wrapper {
  const onStoresChanged = function() {
    this.setState(callback(this.props));
  };

  return compose(
    lifecycle(component => {
      component.state = callback(component.props);
      component.onStoresChanged = onStoresChanged.bind(component);
      component.unsubscribe = Store.observe(stores, component.onStoresChanged);
    }, component => {
      component.unsubscribe();
    })
  );
}

export function beforeMount(callback: ((props: Object) => mixed)): any {
  return (Component: any) => {
    class BeforeMount extends React.Component<any> {
      componentWillMount() {
        callback(this.props);
      }

      render() {
        return createElement(Component, this.props);
      }
    }

    BeforeMount.displayName = `beforeMount/${Component.displayName}`;
    return BeforeMount;
  };
}

export function beforeUnmount(callback: ((props: Object) => mixed)): Wrapper {
  return (Component: any) => {
    class BeforeUnmount extends React.Component<any> {
      componentWillUnmount() {
        callback(this.props);
      }

      render() {
        return createElement(Component, this.props);
      }
    }

    BeforeUnmount.displayName = `beforeUnmount/${Component.displayName}`;
    return BeforeUnmount;
  };
}

export class DefaultLoading extends React.Component<any> {
  componentWillMount() {
    if (this.props.setBreadcrumb) {
      this.props.setBreadcrumb({
        isLoading: true
      });
    }
  }

  componentWillUnmount() {
    if (this.props.setBreadcrumb) {
      this.props.setBreadcrumb({
        isLoading: false
      });
    }
  }

  render() {
    if (this.props.setBreadcrumb) {
      return null;
    }

    return (
      <div className="loading-wrapper">
        加载中
      </div>
    );
  }
}

type KeyPridicate = Array<string> | (key: string) => boolean;

function pick(obj: Object, predicate: KeyPridicate) {
  return Object.keys(obj).filter(key => (
    Array.isArray(predicate) ?
      predicate.includes(key) : predicate(key)
  ));
}

export function renderLoading(LoadingComponent: Function | React.Component<*> = DefaultLoading, predicate: KeyPridicate = () => true): Wrapper {
  const test = (props: Object) => pick(props, predicate).some(
    propName => (props[propName] instanceof Loading)
  );
  const onLoading = () => LoadingComponent;
  const onLoaded = BaseComponent => BaseComponent;

  return compose(branch(test, onLoading, onLoaded));
}

export const onReceiveProps = doOnReceiveProps;
