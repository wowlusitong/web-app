/* @flow */

import * as React from 'react';
import {render} from 'react-dom';

import router from '~/router';

export const start = function() {
  const Router: any = router();
  render(<Router />, document.getElementById('root'));
};
