/* @flow */

import {Record} from 'immutable';

export class Blog extends Record({
  id: '',
  title: '',
  link: '',
  content: '',
  excerpt: ''
}) {
  id: string;
  title: string;
  link: string;
  content: string;
}

export function toBlog(blog: Object): Blog {
  return new Blog({
    id: String(blog.id),
    title: blog.title.rendered,
    link: blog.link,
    content: blog.content.rendered,
    excerpt: blog.excerpt.rendered
  });
}
