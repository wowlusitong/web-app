/* @flow */

import * as React from 'react';
import {Router, browserHistory, applyRouterMiddleware} from 'react-router';

import routes from '~/routes';

let history;

export function goBack() {
  if (!history) {
    return;
  }
  history.goBack();
}

function passingQuery() {
  return {
    renderRouteComponent(child, props) {
      return React.cloneElement(child, {
        query: props.location.query
      });
    }
  };
}

export default function router(): React.Component<any, any> | Function {
  history = browserHistory;

  const HistoryRouter = () => (
    <Router
      history={history}
      render={applyRouterMiddleware(passingQuery())}
      >
      {routes}
    </Router>
  );

  return HistoryRouter;
}
