import * as React from 'react';
import {Route, IndexRedirect} from 'react-router';

import App from '~/components/App';
import Blog from '~/components/Blog';

const routes = (
  <Route path="/" component={App}>
    <IndexRedirect to="/blog" />
    <Route path="blog" component={Blog} />
  </Route>
);

export default routes;
