/* @flow */

import {List} from 'immutable';

import {request} from '~/services/agent';
import {Blog as BlogRecord, toBlog} from '~/models/Blog';

type BlogPayload = {
  blog_id: string;
  title: string;
}
export default class Blog {
  async index(): Promise<List<BlogRecord>> {
    try {
      const res = await request
        .get('https://www.uscreditcardguide.com/wp-json/wp/v2/posts/')
        .end();
      return new List(res.body).map(toBlog);
    } catch (err) {
      throw err.error.message;
    }
  }

  async all(): Promise<Array> {
    try {
      const res = await request
        .get('/api/blogs/')
        .end();
      return res.body;
    } catch (err) {
      return [];
    }
  }

  async getBlogbyID(blogID: string): Promise<number> {
    try {
      const allBlog = await this.all();
      const blog = allBlog.find(b => b.blog_id === blogID);
      if (blog) return blog;
      throw new Error(`can't find blogID: ${blogID}`);
    } catch (err) {
      throw err.error.message;
    }
  }

  async update(payload: BlogPayload) {
    try {
      const blog = await this.getBlogbyID(payload.blog_id);
      await request
        .put(`/api/blogs/${blog.id}/`)
        .send({
          number: blog.number + 1,
          ...payload
        })
        .end();
    } catch (e) {
      await request
        .post('/api/blogs/')
        .send({number: 1, ...payload})
        .end();
    }
  }
}
