/* @flow */

import superAgent from 'superagent';

const superagent: any = superAgent;

const versionWrapper = (() => {
  return function(agent) {
    const origEnd = agent.end;
    agent.end = function(callback) {
      return origEnd.call(agent, res => {
        return callback(res);
      });
    };
  };
})();

const promiseWrapper = function(agent) {
  const origEnd = agent.end;
  agent.end = function(callback) {
    return new Promise((resolve, reject) => {
      origEnd.call(agent, res => {
        if (callback) {
          try {
            resolve(callback(res));
          } catch (err) {
            reject(err);
          }
          return;
        }

        if (res.error) {
          reject(res);
          return;
        }

        resolve(res);
      });
    });
  };
};

const errorWrapper = function(agent: superagent) {
  agent.addErrorHandler = function(callback) {
    agent.on('error', callback);
  };
};

const middleware = function(agent: superagent): void {
  agent.type('json');
  agent.use(versionWrapper);
  agent.use(promiseWrapper);
  agent.use(errorWrapper);
};

// add options request support
superagent.options = function(url: string, data: any, fn: Function) {
  const req = superagent('OPTIONS', url);
  if (typeof data === 'function') {
    fn = data;
    data = null;
  }
  if (data) {
    req.send(data);
  }
  if (fn) {
    req.end(fn);
  }
  return req;
};

const request = function(method: string, url: string): any {
  return superagent(method, url).use(middleware);
};

const methodWrapper = function(method: string) {
  return function(...args): any {
    return superagent[method](...args).use(middleware);
  };
};

request.get = methodWrapper('get');
request.post = methodWrapper('post');
request.del = methodWrapper('del');
request.patch = methodWrapper('patch');
request.put = methodWrapper('put');
request.head = methodWrapper('head');
request.options = methodWrapper('options');

export {request};
