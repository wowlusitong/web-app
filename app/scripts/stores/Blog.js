/* @flow */

import Store from '~/stores/Store';
import {ActionTypes} from '~/Constants';
import Loading from '~/services/Loading';

class Blog extends Store {
  getBlogs() {
    return this.get('blogs') || new Loading();
  }
}

const store = new Blog('blog', {
  blogs: []
});
export default store;

store.addHandler(ActionTypes.LOAD_BLOG_SUCCESS, (cursor, {blogs}) => {
  console.debug('bbb', blogs);
  cursor.set('blogs', blogs);
});
