/* @flow */

import {Map} from 'immutable';
import Cursor from 'immutable/contrib/cursor';
import type {Cursor as CursorType} from 'immutable/contrib/cursor';

export type Payload = Object;
export type Action = {
  type: string
};
export type KeyPath = Array<string>;
export type CursorHandler = (cursor: CursorType, action: Action) => void;
export type Reducer = (state: any, action: Action) => any;

function stateLogger(newState: any, prevState: any, changedPath: KeyPath) {
  const newVal = newState.getIn(changedPath);
  console.info('changed', changedPath, ':', newVal);
}

function preventCursorExpires(currState, prevState, changedPath = []) {
  if (prevState !== currState) {
    throw new Error(`Attempted to alter an expired cursor: ${changedPath.join('/')}`);
  }
}

export function createReducer(
  path: KeyPath,
  handler: CursorHandler
): Reducer {
  return (state: any, action: Action) => {
    let currentState = state;

    const onChange = (newState, prevState, changedPath = []) => {
      if (newState === currentState) {
        return;
      }

      if (process.env.NODE_ENV !== 'production') {
        preventCursorExpires(currentState, prevState, changedPath);
        stateLogger(newState, prevState, changedPath);
      }

      currentState = newState;
    };

    const cursor = Cursor.from(state, path, onChange);
    cursor.withMutations(c => {
      handler(c, action);
    });
    return currentState;
  };
}

export function makeRootReducer() {
  const reducers = [];
  const defaultStates = [];

  function addCursorHandler(path: KeyPath, defaultState: mixed, handler: CursorHandler) {
    reducers.push(createReducer(path, handler));
    defaultStates.push({
      path,
      defaultState
    });
  }

  function rootReducer(state: Map<any, any>, action: Action) {
    while (defaultStates.length > 0) {
      const {path, defaultState} = defaultStates.shift();
      if (process.env.NODE_ENV !== 'production') {
        if (state.hasIn(path)) {
          throw new Error(`path [${path.join(', ')}] already reserved in global redux store`);
        }
      }
      state = state.setIn(path, defaultState);
    }

    if (process.env.NODE_ENV !== 'production') {
      console.groupCollapsed('action', action.type);
      console.info('payload', action);
    }

    state = reducers.reduce((s, reducer) => reducer(s, action), state);

    if (process.env.NODE_ENV !== 'production') {
      console.groupEnd();
    }

    return state;
  }

  return {
    rootReducer,
    addCursorHandler
  };
}
