const webpack = require('webpack');
const WebpackDevServer = require('webpack-dev-server');

const http = require('http');
const express = require('express');
const httpProxy = require('http-proxy');
const url = require('url');

const port = 7070;
const devServerURL = `http://127.0.0.1:${port + 1}`;
const apiServerURL = process.env.API_SERVER_URL || 'http://127.0.0.1:3000';
const publicPath = '/assets';

const createProxyServer = function(endpointURL) {
  return httpProxy.createProxyServer({
    target: endpointURL,
    headers: {
      host: url.parse(endpointURL).host
    }
  }).on('error', err => {
    console.info('Got error on proxy', endpointURL, err);
  });
};

const devServerProxy = createProxyServer(devServerURL);
const apiServerProxy = createProxyServer(apiServerURL);

const app = express();

app.use(publicPath, (req, res) => {
  req.url = publicPath + req.url;
  devServerProxy.web(req, res);
});

app.use('/api', (req, res) => {
  req.url = `${req.url}`;
  apiServerProxy.web(req, res);
});

app.use('/*', (req, res) => {
  req.url = `${publicPath}/index.html`;
  devServerProxy.web(req, res);
});

const config = require('./webpack.config');
config.entry.main = [
  'webpack-dev-server/client?/',
  'webpack/hot/dev-server'
].concat(config.entry.main);

config.devtool = 'eval';
config.output.pathinfo = true;
config.output.publicPath = `${publicPath}/`;
config.plugins.push(new webpack.HotModuleReplacementPlugin());

new WebpackDevServer(webpack(config), {
  hot: true,
  quiet: true,
  publicPath: config.output.publicPath,
  stats: {colors: true}
}).listen(port + 1, '127.0.0.1');

const server = http.createServer(app);
server.on('upgrade', (req, socket, head) => {
  if (req.url.startsWith('/api/')) {
    apiServerProxy.ws(req, socket, head);
  } else {
    devServerProxy.ws(req, socket, head);
  }
});

server.listen(port, (err) => {
  if (err) {
    throw err;
  }
  console.info('Running dev server on port', port);
  console.info('proxying /api/* to', apiServerURL);
});
