const join = require('path').join;
const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const ExtractTextPlugin = require('extract-text-webpack-plugin');

const plugins = [
  new webpack.NoEmitOnErrorsPlugin(),
  new HtmlWebpackPlugin({
    title: '',
    favicon: '',
    template: 'index.html',
    filename: 'index.html',
    inject: false,
    env: process.env.NODE_ENV
  }),
  new webpack.DefinePlugin({
    'process.env': {
      NODE_ENV: JSON.stringify(process.env.NODE_ENV || 'production')
    }
  })
];

if (process.env.NODE_ENV === 'production') {
  plugins.push(
    new webpack.optimize.ModuleConcatenationPlugin(),
    new webpack.LoaderOptionsPlugin({
      minimize: true
    }),
    new webpack.optimize.UglifyJsPlugin({
      compress: {
        warnings: false
      },
      sourceMap: true
    }),
    new webpack.optimize.OccurrenceOrderPlugin(),
    new ExtractTextPlugin('[contenthash].css'),
  );
}

const config = {
  context: join(__dirname, '/app'),
  devtool: 'hidden-source-map',
  entry: {
    main: ['./styles/main.css', './scripts/main.js']
  },
  node: {
    buffer: false,
    __filename: true
  },
  output: {
    path: join(__dirname, '/dist/assets'),
    publicPath: '/',
    filename: '[name]_[hash].js',
    chunkFilename: '[id].[chunkhash].bundle.js',
    library: 'App',
    libraryTarget: 'umd'
  },
  module: {
    rules: [
      {
        test: /\.js$/,
        loader: 'babel-loader',
        query: {
          cacheDirectory: true
        },
        include: /app\/scripts/
      },
      {
        test: /\.json$/,
        loader: 'json-loader'
      },
      {
        test: /\.css$/,
        use: process.env.NODE_ENV === 'production' ?
          ExtractTextPlugin.extract({
            fallback: 'style-loader',
            use: ['css-loader', 'postcss-loader']
          })
          :
          ['style-loader', 'css-loader', 'postcss-loader']
      },
      {
        test: /\.(eot|woff|woff2|ttf|svg|png|jpg)($|\?)/,
        loader: 'file-loader'
      }
    ]
  },
  resolve: {
    modules: [path.resolve(__dirname, 'node_modules'), 'node_modules']
  },
  plugins
};

module.exports = config;
